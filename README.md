# IPv6.br: Curso Básico de IPv6

Uma alternativa mais leve a [VM para Laboratórios](http://ipv6.br/downloads/CursoIPv6br-CORE4.6-20150318.ova) do [curso básico de IPv6 do cgi.br/nic.br](http://ipv6.br/pagina/curso-basico-ead).

## Requisitos

* Conexão com à internet.
* Git (https://git-scm.com/)
* Um container engine (Docker, Podman, etc)

## Como usar

### Obter os arquivos

Clone o "projeto" e entre no diretório.

```console
git clone https://gitlab.com/tiagorocha/ipv6br-curso-basico.git
cd ipv6br-curso-basico
```

> Obs: É possível baixar um arquivo compactado com o projeto.

### Rodar um container do CORE EMU

Abaixo vemos um exemplo usando o podman como ferramenta de containers. Para usar Docker basta substituir `podman` por `docker` na linha de comando.

```console
podman run -d --cap-add=NET_ADMIN --cap-add=SYS_ADMIN -v $(pwd)/shared:/root/shared -p 5900:5900 -p 8080:8080 docker.io/d3f0/coreemu_vnc
```

Após iniciar o container acesse [localhost:8080](http://localhost:8080/) no browser ou `localhost:5900` com um software cliente VNC. Use *coreemu* como senha.

> Obs: Atenção para o caminho dos Laboratórios. No container os arquivos estão no diretório `/root/shared/lab`.

### Parar e remover o container

```console
podman container ls -a
podman container stop ContainerID_ou_Nome
podman container rm ContainerID_ou_Nome
```

### Remover a imagem de container

```console
podman image rm docker.io/d3f0/coreemu_vnc
```

> Obs: Lembre-se que uma imagem só pode ser removida se não existirem containers criados a partir dela.

## Wireshark

### Instalar Wireshark no container

1. Acesse o dekstop do container (via browser ou cliente VNC).
2. Minimize a janela do CORE e clique com o botão direito do mouse no desktop.
3. Clique em "Terminal emulator".
4. Instale o Wireshark com a linha de comando `apt update && apt install wireshark`.
5. Abra o programa rodando `wireshark` no próprio terminal.

> Obs: Lembre-se que modificações no ambiente do containers são efêmeras, elas existem apenas enquanto o container existir. Para uma "instalação permanente" do Wireshark crie uma imagem customizada, veja mais detalhes na [documentação do Docker](https://docs.docker.com/engine/reference/builder/).

### Usando o Wireshark do host

É possível usar um Wireshark instalado fora do container. Para isso copie/salve os arquivos de tcpdump no diretório "compartilhado" (`/root/shared`) com o container e depois abra o(s) arquivo(s) no Wireshark do host.

## Referências

http://ipv6.br/pagina/curso-basico-ead

http://ipv6.br/pagina/livro-ipv6/

https://coreemu.github.io/core/

https://github.com/stuartmarsden/dockerCoreEmu

https://github.com/D3f0/coreemu_vnc

http://eriberto.pro.br/core/
